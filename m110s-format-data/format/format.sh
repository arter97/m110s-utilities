#!/sbin/sh
#
# format SHW-M110S /data partition, /data/data mtd partition for a successful CyanogenMod installation
# 1.0 (c) 2014 by Prizm
# sktjdgns1189, sch2307, arter97
#

# use lazy unmount to force unmounting on all cases
umount -l /data/data
umount -l /data

# format /data to f2fs, mount it and create /data/data for mtd
/tmp/mkfs.f2fs -l USERDATA /dev/lvpool/userdata
mount -t f2fs /dev/lvpool/userdata /data
mkdir /data/data
chmod 771 /data/data

# format /data/data with erase_image and mount it
/tmp/erase_image datadata
mount -t yaffs2 /dev/block/mtdblock2 /data/data

# enjoy :D
sync

#!/sbin/sh
#
# format SHW-M110S /system or /system, /data partition for a successful CyanogenMod installation
# 1.0 (c) 2014 by Prizm
# sktjdgns1189, sch2307, arter97
#

# setprop to GT-I9000 to flash galaxysmtd.zip without assertion errors
setprop ro.product.device GT-I9000
setprop ro.build.product GT-I9000

# block reading /sys/class/mtd/mtd2/size to prevent detecting the device as running on a mtd (old) device
chmod 000 /sys/class/mtd/mtd2/size

# remove /dev/block/mtdblock0 to prevent detecting the device as running on a mtd (current) device
# we should create this nod later as we need to flash kernel
rm /dev/block/mtdblock0

# we are now ready to bypass all the 'if's from the ROM's updater.sh

# now repartition /dev/block/mmcblk0p2 to lvm
# if userdata lvm partition does not exists, proceed with repartitioning
# otherwise, repartitioning is not necessary
if [[ ! -e /dev/lvpool/userdata ]] ; then
# use lazy unmount to force unmounting on all cases
	umount -l /data
	umount -l /system

# try to remove already existing lvm partition
	/tmp/lvm lvremove -f lvpool
	/tmp/lvm pvcreate /dev/block/mmcblk0p2
	/tmp/lvm vgcreate lvpool /dev/block/mmcblk0p2

# use SYSTEM_SIZE same as CyanogenMod 11 suggests
	/tmp/lvm lvcreate -L 629145600B -n system lvpool
	/tmp/lvm lvcreate -l 100%FREE -n userdata lvpool

# we now have a raw userdata partition, format to f2fs and mount it
	/tmp/mkfs.f2fs -l USERDATA /dev/lvpool/userdata
	mount -t f2fs /dev/lvpool/userdata /data
fi

# use lazy unmount to force unmounting on all cases
umount -l /system
/tmp/mkfs.f2fs -l SYSTEM /dev/lvpool/system

# remove /system directory and symlink to /system_f2fs to prevent unmounting real system via updater-script
rm -rf /system
mkdir /system_f2fs
mount -t f2fs /dev/lvpool/system /system_f2fs
ln -s /system_f2fs /system

# remove /dev/lvpool/system to prevent formatting it back to ext4 via updater-script
rm -f /dev/lvpool/system

# we are now ready to flash galaxysmtd.zip with f2fs file-system maintained
# enjoy :D
sync
